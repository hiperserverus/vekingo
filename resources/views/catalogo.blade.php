@extends('layouts.app')

@section('content')

 
    <!-- Breadcrumbs --> 

       @include('layouts.breadcrumbs')

        <!-- /Breadcrumbs --> 

            <!-- Productos --> 

       @include('layouts.container')

        <!-- /Productos --> 
        
               <!-- Modal --> 

        @include('layouts.modal')

         <!-- /Modal --> 

         

@endsection

