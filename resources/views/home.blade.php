@extends('layouts.app')

@section('content')

        <!-- Carousel -->

        <div id="carousel-container">


            <div id="productosCarousel"  class=" carousel slide" data-ride="carousel">
              

              <ol class="carousel-indicators">
                <li data-target="#productosCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#productosCarousel" data-slide-to="1"></li>
                <li data-target="#productosCarousel" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                  <img class="d-block img-fluid" src="{{url('/images/producto1.svg')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                                    <h4>First slide</h4>
                                    <p class="hidden-sm-down">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                  </div>
                </div>
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{url('/images/producto1.svg')}}" alt="Second slide">
                                    <div class="carousel-caption d-none d-md-block">
                                    <h4>Second slide</h4>
                                    <p class="hidden-sm-down">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                  </div>
                </div>
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="{{url('/images/producto1.svg')}}" alt="Third slide">
                                    <div class="carousel-caption d-none d-md-block">
                                    <h4>Third slide</h4>
                                    <p class="hidden-sm-down">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                  </div>
                </div>
              </div>
              <a class="left carousel-control" href="#productosCarousel" role="button" data-slide="prev">
                <span class="icon-prev" aria-hidden="true"></span>
                <span class="sr-only">Producto anterior</span>
              </a>
              <a class="right carousel-control" href="#productosCarousel" role="button" data-slide="next">
                <span class="icon-next" aria-hidden="true"></span>
                <span class="sr-only">Producto siguiente</span>
              </a>
            </div>

        </div>

        <!-- /Carousel -->

       <!-- Info --> 

        @include('layouts.info')

        <!-- /Info --> 

@endsection
