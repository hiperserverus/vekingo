<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Vekingo') }}</title>

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap-flex.min.css" rel="stylesheet" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        
        <link rel="stylesheet" href="{{ url('/css/app.css') }}">

            <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    </head>
    <body>

        <!-- Header -->

        <header id="header-container">
                <div class="container">
                        <div class="row flex-items-xs-middle flex-items-xs-between">
                            <div class="col-xs-3 ">
                                <h1>Vekingo</h1>
                            </div>
                            <div class="col-xs-3 text-xs-right ">
                            <button class="navbar-toggler hidden-sm-up" data-toggle="collapse" data-target="#navMenu">
                                &#9776;
                            </button>
                    
                    <!-- login -->

                        @if (Auth::guest())
                            <a class="login hidden-xs-down text-uppercase font-weight-bold" href="{{ url('/login') }}" >Entrar</a>
                            <a class="login hidden-xs-down text-uppercase font-weight-bold"  href="{{ url('/register') }}">Registrarse</a>
                        @else
                        
                               <a class="login hidden-xs-down text-uppercase font-weight-bold"  href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       Salir
                               </a>

                              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                 {{ csrf_field() }}
                               </form>
                         
                        @endif
            <!-- /login -->
                            </div>
                        </div>
                </div>
        </header>

        <!-- /Header -->

        <!-- Menu -->

        <div id="menu-container">


            <nav id="navMenu" class="navbar-toggleable-xs navbar navbar-light collapse">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-10 offset-xs-1 col-md-4 offset-md-0">
                                    <ul class="nav navbar-nav">
                                        <li class="nav-item text-xs-center active">
                                            <a href="{{url('/')}}" class="nav-link">Home</a>
                                        </li>
                                        <li class="nav-item text-xs-center">
                                            <a href="{{url('/catalogo')}}" class="nav-link">Catálogo</a>
                                        </li>
                                        <li class="nav-item text-xs-center">
                                            <a href="#" class="nav-link">Carrito</a>
                                        </li>
                                        <li class="nav-item text-xs-center hidden-sm-up">
                                                                <!-- login -->

                        @if (Auth::guest())
                            <a class="nav-link"  href="{{ url('/login') }}" >Entrar</a>
                            <a class="nav-link"  href="{{ url('/register') }}">Registrarse</a>
                        @else
                        
                               <a class="nav-link"  href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       Salir
                               </a>

                              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                 {{ csrf_field() }}
                               </form>
                         
                        @endif
            <!-- /login -->
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-6 offset-md-2 hidden-xs-down">
                                    <form>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="¿Encontro lo que buscaba?">
                                            <span class="input-group-btn">
                                                <button class="btn btn-platzi" type="button">
                                                <span class="hidden-sm-down">Buscar</span>
                                                <i class="fa fa-search hidden-md-up"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </nav>
        <div class="search-bar container hidden-sm-up">
            <div class="row">
                <div class="col-xs">

                                    <form>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="¿Encontro lo que buscaba?">
                                            <span class="input-group-btn">
                                                <button class="btn btn-platzi" type="button">
                                                <span class="hidden-sm-down">Buscar</span>
                                                <i class="fa fa-search hidden-md-up"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>

                </div>
            </div>
        </div>

        </div>
        
        

        <!-- /Menu -->

         @yield('content')


        <!-- Footer -->

        <footer id="footer-container">
            <div class="container">
                <div class="row text-xs-center text-md-left">
                    <div class="col-md-4">
                        <form id="suscribeForm" action="#" method="POST">
                            <h4 class="text-uppercase">¿Quieres recibir todas las novedades?</h4>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email"
                                placeholder="tu@correo.com">
                                <small class="form-text text-muted">No compartiremos tu informacion con nadie mas</small>
                            </div>
                            <button type="submit" class="btn btn-primary form-text">Suscribirme</button>
                        </form>
                    </div>
                    <div class="col-md-3 offset-md-5">
                        <h4>Navegacion</h4>
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="{{url('/')}}" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('/catalogo')}}" class="nav-link">Catálogo</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Carrito</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Login</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">Contacto</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- /Footer -->



        <!-- jQuery first, then Tether, then Bootstrap JS. -->

<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha256-j+XrvkQ4jJ99Hj0pJKPr6k0RCgxDDSTs3PBqLrX2EMc=" crossorigin="anonymous"></script>


<script src="{{url('/js/app.js')}}"></script>

    </body>
</html>