					<!-- Productos -->
					<div class="col-xs-12 col-md-9 listado-productos">
						<div class="row">


							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-lg-4">
								<div class="card">
									<img src="{{url('/images/producto.jpg')}}" alt="Producto 1" class="card-img-top img-fluid">
									<div class="card-block">
										<h4 class="card-tittle">Producto 1</h4>
										<p class="card-text">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</div>
								</div>
							</div>





						</div>

						<div class="row">
							<div id="paginator-container" class="col-xs text-xs-center">
								<nav>
									<ul class="pagination">
										<li class="page-item disabled">
											<a href="#" class="page-link">
												&laquo;
											</a>
										</li>
										<li class="page-item active">
											<a href="#" class="page-link">
												1
											</a>
										</li>
										<li class="page-item">
											<a href="#" class="page-link">
												2
											</a>
										</li>
										<li class="page-item">
											<a href="#" class="page-link">
												3
											</a>
										</li>
										<li class="page-item">
											<a href="#" class="page-link">
												4
											</a>
										</li>
										<li class="page-item">
											<a href="#" class="page-link">
												5
											</a>
										</li>
										<li class="page-item">
											<a href="#" class="page-link">
												&raquo;
											</a>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
					<!-- /Productos -->