
<!-- Info -->
			 <div id="info-container">
			 	<div class="container">
			 		<div class=" row text-xs-center">
			 			<div class=" col-xs-12 col-md-4">
			 			<img src="{{url('/images/calidad.png')}}" alt="calidad" class="img-fluid">
			 				<h4>Calidad</h4>
			 			</div>
			 			<div class=" col-xs-12 col-md-4">
			 			<img src="{{url('/images/envio.png')}}" alt="envio" class="img-fluid">
			 				<h4>Envio gratis</h4>
			 			</div>
			 			<div class=" col-xs-12 col-md-4">
			 			<img src="{{url('/images/soporte.png')}}" alt="soporte" class="img-fluid">
			 				<h4>Soporte 24hs</h4>
			 			</div>
			 		</div>
			 	</div>
			 </div>
		<!-- /Info -->