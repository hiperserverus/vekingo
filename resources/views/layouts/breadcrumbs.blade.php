	<!-- Breadcrumbs -->

		<section id="breadcrumbs-container">
			<div class="container">
				<div class="row">
					<div class="col-xs">
						<nav class="breadcrumb">

							<a href="{{url('/catalogo')}}" class="breadcrumb-item">Catálogo</a>
							<span class="breadcrumb-item active">Todos los productos</span>
						</nav>
					</div>
				</div>
			</div>
		</section>

		<!-- /Breadcrumbs -->