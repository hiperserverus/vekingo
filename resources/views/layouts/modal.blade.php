				<!-- Modal -->

		<div class="modal fade" id="modalOferta">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						&times;
					</button>
						<h4 class="modal-title">Oferta exclusiva</h4>
					</div>
					<div class="modal-body">
						Regístrate hoy y recibe 15$ de descuento!
						<form action="#" class="form">
							<div class="form-group">
								<input type="email" class="form-control" id="email" name="email" placeholder="tu@correo.com">
								<small class="form-text text-muted">No compartiremostu información con nadie más</small>
							</div>
							<button type="submit" class="btn btn-platzi form-text">Regístrame</button>
							<button id="btnNoRegistrar" class="btn btn-warning form-text" data-dismiss="modal">No, no quiero el descuento</button>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- /Modal -->